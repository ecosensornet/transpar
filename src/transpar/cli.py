"""
Listen/Write on specific serial port to retrieve messages/send commands
"""
from datetime import datetime
from pathlib import Path
from threading import Thread

import serial


def task_save_event(post_data, pth_log):
    """Write event on disk

    Args:
        post_data (bytes): json formatted string
        pth_log (Path): log file to store events

    Returns:
        None
    """
    now = datetime.utcnow()
    with open(pth_log, 'a') as fhw:
        fhw.write(f"{now.isoformat()}->{post_data.decode('utf-8').strip()}\n")


def open_port(port):
    """Open serial port for reading

    Args:
        port: name of serial port

    Returns:
        (serial object): already open
    """
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = 115200
    ser.timeout = 1  # [s]
    # ser.rts = False
    # ser.dtr = False
    ser.open()  # need to do it exactly like that to ensure no reset signal is send

    return ser


class Gateway(Thread):
    def __init__(self, port, save_dir="res"):
        super().__init__()

        self._port = port
        self._save_dir = Path(save_dir)
        self._save_dir.mkdir(exist_ok=True, parents=True)

        self._running = False

        self._msg_to_send = []

        self._current_log_ind = 0
        logs = sorted(self._save_dir.glob("log_*.log"))
        if len(logs) > 0:
            last_log = logs[-1].name[:-4]
            self._current_log_ind = int(last_log.split("_")[1])

        self._pth_log = self._save_dir / f"log_{self._current_log_ind:04d}.log"

        if len(logs) > 0:
            self._entry_nb = len(self._pth_log.open().readlines())
        else:
            self._entry_nb = 0

    def stop(self):
        """Stop gateway and close everything

        Returns:
            (None)
        """
        self._msg_to_send.clear()
        self._running = False

    def send(self, msg):
        """Send msg to node

        Args:
            msg (str): whatever

        Returns:
            (None)
        """
        if not isinstance(msg, bytes):
            msg = msg.encode('utf-8')

        self._msg_to_send.append(msg)

    def scan(self):
        """Launch single scan

        Returns:
            (None)
        """
        self.send('start')

    def run(self):
        ser = open_port(self._port)
        self._running = True

        while self._running:
            # send available message if any
            while len(self._msg_to_send) > 0:
                msg = self._msg_to_send.pop(0)
                ser.write(msg)

            # read one line for incoming message
            line = ser.readline()
            if line:
                if line.startswith(b"JSON:"):
                    task_save_event(line[6:], self._pth_log)
                    self._entry_nb += 1
                    if self._entry_nb >= 10000:
                        self._current_log_ind += 1
                        self._pth_log = self._save_dir / f"log_{self._current_log_ind:04d}.log"
                        self._entry_nb = 0
                else:
                    print(f"msg ({datetime.utcnow().time().isoformat()}): {line}")
                    if line.startswith(b"MAC:"):
                        mac_hex = line[5:].strip()
                        mac_dec = [int(mac_hex[i * 2:i * 2 + 2], 16) for i in range(6)]
                        print("dec", mac_dec)

        ser.close()
