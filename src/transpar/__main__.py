import sys

from serial.tools.list_ports import comports

from .cli import Gateway

try:
    arg_port = sys.argv[1]
except IndexError:
    arg_port = 'COM3'

if arg_port == "ls":  # list all ports
    for port in comports():
        print("port", port.name)
else:
    gate = Gateway(arg_port)
    gate.start()


def scan():
    gate.send('start')


def end():
    gate.stop()
    sys.exit(0)
