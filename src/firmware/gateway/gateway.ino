#include <esp_now.h>
#include <WiFi.h>

#include "src/settings.h"

uint8_t maca[6];
esp_now_peer_info_t node_info;
bool msg_send;
uint8_t node_ip[6] = {0x84, 0xf7, 0x03, 0xea, 0xfb, 0x2e};

unsigned long last_blip;

void display_mac() {
  char buffer[18];
  sprintf(buffer,
          "MAC: %02x%02x%02x%02x%02x%02x",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]
         );
  Serial.println(buffer);
}


// Callback function that will be executed when data is received
void on_data_received(const uint8_t* mac, const uint8_t* data, int len) {
  Serial.print("JSON: ");
  String msg = (char*) data;
  Serial.println(msg);
}


void on_data_sent(const uint8_t* mac_addr, esp_now_send_status_t status) {
  msg_send = true;
  Serial.print("Gate Last Packet Send Status: ");
  if (status == ESP_NOW_SEND_SUCCESS) {
    Serial.println("Delivery success");
  }
  else {
    Serial.print("Delivery fail (");
    Serial.print(status);
    Serial.print(") to: ");
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(*mac_addr, HEX);
      mac_addr ++;
      Serial.print(", ");
    }
    Serial.println("");
  }
}


void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  last_blip = millis();

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  delay(2000);
  while (!Serial) {
    delay(100);
  }

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  } else {
    WiFi.macAddress(maca);

    display_mac();
  }

  esp_now_register_recv_cb(on_data_received);
  esp_now_register_send_cb(on_data_sent);

  // register node
  memcpy(node_info.peer_addr, node_ip, 6);
  node_info.channel = 0;
  node_info.encrypt = false;

  if (esp_now_add_peer(&node_info) != ESP_OK) {
    uint8_t* mac_addr = node_ip;
    Serial.print("Failed to register peer: ");
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(*mac_addr, HEX);
      mac_addr ++;
      Serial.print(", ");
    }
    Serial.println("");
  }
}

void loop() {
  if (Serial.available() > 0) {
    String msg = Serial.readString();
    if (msg == "start") {
      //       Serial.println("SEND CMD");
      msg_send = false;
      esp_now_send(node_ip, (uint8_t*) msg.c_str(), msg.length() + 1);
      while (!msg_send) {
        //         Serial.println("waiting send");
        delay(20);
      }
    }
    else if (msg == "mac") {
      display_mac();
    }
    else {
      Serial.print("msg not recognized as cmd: ");
      Serial.println(msg);
    }
  }
  else {
    if (millis() - last_blip > 1000) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      last_blip = millis();
    }
    else {
      delay(100);
    }
  }
}
