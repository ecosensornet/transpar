#ifndef MEASURE_H
#define MEASURE_H

#include "../settings.h"

namespace measure {

typedef struct {
  uint8_t chn;
  int32_t pos;
  uint32_t irr;
  float irr_cal;
} Sample;

void setup();
void reset_pos();
void sample(uint8_t ind);
void scan(uint32_t scan_ind);

void fmt_meas(String& msg, uint32_t scan_ind);

}  // namespace measure

extern measure::Sample sample_cur;

#endif //  MEASURE_H
