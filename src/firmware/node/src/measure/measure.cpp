#include <ArduinoJson.h>
#include <Wire.h>
#include <component_mcp3424.h>

#include "../calibration/calibration.h"
#include "../espnow_com/espnow_com.h"

#include "measure.h"

#define I2C_SCL 39
#define I2C_SDA 37

#define MOT_DIR 33
#define MOT_ENA 18
#define MOT_PUL 35

#define END_FWD 12
#define END_BWD 11

#define WAITING 0
#define STARTING 1
#define MOVING 2
#define STOPPED 3
#define FWD 0
#define BWD 1


measure::Sample sample_cur;

byte move_dir = FWD;
byte state = WAITING;

TwoWire i2c = TwoWire(0);
MCP3424 light = MCP3424();
MCP3424::Config status;
long value;
uint8_t err;
int32_t current_step;


hw_timer_t* timer = NULL;
byte step_state = LOW;


void IRAM_ATTR stepper_timer(){  // interrupt used to send on step
  if (state == MOVING) {
    if (step_state == LOW) {
      step_state = HIGH;
      digitalWrite(MOT_PUL, HIGH);
        if (move_dir == FWD) {
          current_step ++;
        } else {
          current_step --;
        }
    }
    else {
      step_state = LOW;
      digitalWrite(MOT_PUL, LOW);
    }
  }
}


void IRAM_ATTR isr() {
  if (state == MOVING) {
#if (DEBUG == 1)
    Serial.println("isr");
#endif // DEBUG
    if (move_dir == FWD & digitalRead(END_FWD) == LOW) {
#if (DEBUG == 1)
      Serial.println("end fwd");
#endif // DEBUG
      state = STOPPED;
      move_dir = BWD;
      digitalWrite(MOT_DIR, LOW);
    }
    else if (move_dir == BWD & digitalRead(END_BWD) == LOW) {
#if (DEBUG == 1)
      Serial.println("end bwd");
#endif // DEBUG
      state = STOPPED;
      move_dir = FWD;
      digitalWrite(MOT_DIR, HIGH);
    }
  }
}


void measure::setup() {
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();

  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &stepper_timer, true);

  // configure IO
  pinMode(END_FWD, INPUT_PULLUP);
  pinMode(END_BWD, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(END_FWD), isr, FALLING );
  attachInterrupt(digitalPinToInterrupt(END_BWD), isr, FALLING );

  pinMode(MOT_DIR, OUTPUT);
  pinMode(MOT_ENA, OUTPUT);
  pinMode(MOT_PUL, OUTPUT);

  digitalWrite(MOT_ENA, HIGH);

  light.begin(i2c);
  delay(1); // MC3424 needs 300us to settle, wait 1ms

  state = WAITING;

  // timerAlarmWrite(timer, 700, true);  // res_18 micro step 16 (3200 pulse per revolution), approx 4 conversions per cm
  timerAlarmWrite(timer, 210, true);  // res_16 micro step 16 (3200 pulse per revolution), approx 4 conversions per cm
  timerAlarmEnable(timer);
}

void measure::reset_pos() {
  if (digitalRead(END_FWD) == LOW) {
    move_dir = BWD;
  } else if (digitalRead(END_BWD) == LOW) {
    move_dir = FWD;
  } else {
    move_dir = FWD;  // default
  }
  current_step = 0;  // TODO reset position of moving part to ensure this in default case
  if (move_dir == FWD) {
    digitalWrite(MOT_DIR, HIGH);
  } else {
    digitalWrite(MOT_DIR, LOW);
  }
}


void measure::sample(uint8_t ind){
    MCP3424::Channel meas_chn = MCP3424::chn_1;

    sample_cur.chn = ind + 1;
    sample_cur.pos = current_step;

    switch (ind) {
      case 0:
        meas_chn = MCP3424::chn_1;
        break;
      case 1:
        meas_chn = MCP3424::chn_2;
        break;
      case 2:
        meas_chn = MCP3424::chn_3;
        break;
      case 3:
        meas_chn = MCP3424::chn_4;
        break;
      default:
        // statements
        break;
    }
    // read light
    err = light.convert_and_read(meas_chn, MCP3424::one_shot, MCP3424::res_16, MCP3424::gain_8, 1000000, value, status);
    if (!err) {
        sample_cur.irr = value;
        sample_cur.irr_cal = calib.irr[ind] * (float)value;
    }
}


void measure::scan(uint32_t scan_ind){
    reset_pos();
    state = MOVING;
    digitalWrite(MOT_ENA, LOW);
    String msg = "{\"scan\": " + String(scan_ind) + ", \"msg\": \"BEGIN SCAN\"}";
    espnow_com::send_msg(&msg);
#if (DEBUG == 1)
      Serial.println(msg);
#endif // DEBUG
      for (uint8_t i = 0; (i < 400)&(state == MOVING); i++) {
          sample(i % 4);
          msg = "";
          fmt_meas(msg, scan_ind);
          espnow_com::send_msg(&msg);
#if (DEBUG == 1)
          Serial.println(msg);
#endif // DEBUG
      }
      msg = "{\"scan\": " + String(scan_ind) + ", \"msg\": \"END SCAN\"}";
      espnow_com::send_msg(&msg);
#if (DEBUG == 1)
      Serial.println(msg);
#endif // DEBUG
    digitalWrite(MOT_ENA, HIGH);
    state=STOPPED;
}


void measure::fmt_meas(String& msg, uint32_t scan_ind) {
  StaticJsonDocument<512> doc;
  
  doc["scan"] = scan_ind;
  doc["chn"] = sample_cur.chn;
  doc["pos"] = sample_cur.pos;
  doc["irr"] = sample_cur.irr;
  doc["irr_cal"] = sample_cur.irr_cal;

  serializeJson(doc, msg);
}
