#include <ArduinoJson.h>
#include <LittleFS.h>

#include "calibration.h"

Calibration calib;

void calibration::setup() {
  LittleFS.begin();
  calibration::load_file();
}


void calibration::load_file() {
  uint8_t i;
  StaticJsonDocument<512> doc;
  File fhr = LittleFS.open("/calib.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  for (i = 0; i < 4; i++) {
    calib.irr[i] = doc["irr"]["coeffs"][i];
  }

  yield();
}


void calibration::save_file() {
  uint8_t i;
  StaticJsonDocument<512> doc;

  JsonArray irr_coeffs = doc["irr"].createNestedArray("coeffs");
  for (i = 0; i < 4; i++) {
    irr_coeffs.add(calib.irr[i]);
  }

  File fhw = LittleFS.open("/calib.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  yield();
}
