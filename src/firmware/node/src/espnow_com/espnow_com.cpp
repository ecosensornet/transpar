#include <esp_now.h>
#include <WiFi.h>

#include "espnow_com.h"

esp_now_peer_info_t gateway_info;

uint8_t gate_ip[6] = {0x84, 0xf7, 0x03, 0xea, 0xf2, 0x24};
uint8_t maca[6];

uint8_t last_cmd;
uint8_t msg_status;

// Callback function that will be executed when data is received
void espnow_com::on_data_received(const uint8_t* mac, const uint8_t* data, int len) {
  String msg = (char*) data;
#if (DEBUG == 1)
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("msg: ");
  Serial.println(msg);
#endif // DEBUG
  if (msg == "start") {
    last_cmd = CMD_START;
  }
}


void espnow_com::on_data_sent(const uint8_t* mac_addr, esp_now_send_status_t status) {
  msg_status = MSG_SENT;
#if (DEBUG == 1)
  Serial.print("Last Packet Send Status: ");
  if (status == ESP_NOW_SEND_SUCCESS) {
    Serial.println("Delivery success");
  }
  else {
    Serial.print("Delivery fail (");
    Serial.print(status);
    Serial.print(") to: ");
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(*mac_addr, HEX);
      mac_addr ++;
      Serial.print(", ");
    }
    Serial.println("");
  }
#endif // DEBUG
}


void espnow_com::setup() {
#if (DEBUG == 1)
  Serial.println("setup local espnow");
#endif // DEBUG

  WiFi.mode(WIFI_STA);
#if (DEBUG == 1)
  char buffer[18];
  WiFi.macAddress(maca);
  sprintf(buffer,
          "MAC: %02x%02x%02x%02x%02x%02x",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]
         );
  Serial.println(buffer);
#endif // DEBUG

  if (esp_now_init() != ESP_OK) {
#if (DEBUG == 1)
    Serial.println("Error initializing ESP-NOW");
#endif // DEBUG
    return;
  }

  esp_now_register_send_cb(on_data_sent);
  esp_now_register_recv_cb(on_data_received);

  // register gateway
  memcpy(gateway_info.peer_addr, gate_ip, 6);
  gateway_info.channel = 0;
  gateway_info.encrypt = false;

  if (esp_now_add_peer(&gateway_info) != ESP_OK) {
#if (DEBUG == 1)
    uint8_t* mac_addr = gate_ip;
    Serial.print("Failed to register peer: ");
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(*mac_addr, HEX);
      mac_addr ++;
      Serial.print(", ");
    }
    Serial.println("");
#endif // DEBUG
  }

  last_cmd = CMD_NONE;
  msg_status = MSG_SENT;
}


void espnow_com::send_msg(String* msg) {
#if (DEBUG == 1)
  Serial.println("beg of send_msg");
#endif // DEBUG

  msg_status = MSG_IN_PIPE;
  esp_now_send(gate_ip, (uint8_t*) msg->c_str(), msg->length() + 1);

    while (msg_status != MSG_SENT) {
#if (DEBUG == 1)
      Serial.println("waiting send");
#endif // DEBUG
      delay(20);
    }

#if (DEBUG == 1)
  Serial.println("end of send_msg");
#endif // DEBUG
}
