#ifndef ESPNOW_COM_H
#define ESPNOW_COM_H

#include <esp_now.h>

#include "../settings.h"

#define CMD_NONE 0
#define CMD_START 1

#define MSG_IN_PIPE 0
#define MSG_SENT 1

extern uint8_t last_cmd;
extern uint8_t msg_status;

namespace espnow_com {

void on_data_received(const uint8_t*, const uint8_t*, int);
void on_data_sent(const uint8_t*, esp_now_send_status_t);
void setup();
void send_msg(String* msg);

}  // namespace mode_espnow

#endif //  ESPNOW_COM_H
