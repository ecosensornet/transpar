#include "src/calibration/calibration.h"
#include "src/settings.h"
#include "src/espnow_com/espnow_com.h"
#include "src/measure/measure.h"

uint32_t current_scan = 0;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

#if (DEBUG == 1)
  // Initialize Serial Monitor
  Serial.begin(115200);
  while (!Serial) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(10);
    digitalWrite(LED_BUILTIN, LOW);
    delay(20);
  }
#endif // DEBUG

  espnow_com::setup();
  calibration::setup();
  measure::setup();

#if (DEBUG == 1)
  Serial.println("end setup");
#endif // DEBUG
}

void loop() {
  if (last_cmd == CMD_NONE) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
  }
  else if (last_cmd == CMD_START) {
    last_cmd = CMD_NONE;
#if (DEBUG == 1)
    Serial.println("starting now");
#endif // DEBUG
    measure::scan(current_scan);
    current_scan ++;
  }
  else {
#if (DEBUG == 1)
    Serial.println("bad command");
#endif // DEBUG
    delay(1000);
  }
}
