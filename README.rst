========================
transpar
========================

.. {# pkglts, doc

.. image:: https://ecosensornet.gitlab.io/transpar/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/transpar/0.0.1/

.. image:: https://ecosensornet.gitlab.io/transpar/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/transpar

.. image:: https://ecosensornet.gitlab.io/transpar/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://ecosensornet.gitlab.io/transpar/

.. image:: https://badge.fury.io/py/transpar.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/transpar

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/ecosensornet/transpar/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/ecosensornet/transpar/commits/main

.. |main_coverage| image:: https://gitlab.com/ecosensornet/transpar/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/ecosensornet/transpar/commits/main
.. #}

PAR transect measuring tools

